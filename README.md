# React-webpack-karma SSR boilerplate

## Start

```
npm install
npm start         # start server
npm run start-hot # start server with php-hot-deploy
```

## Test

```
npm run clean # cleanup
npm run build # build js/css
npm test      # start karma server
npm run lint  # lint airbnb jscs
```
